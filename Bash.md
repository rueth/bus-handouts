# Einstieg in die Shell-Programmierung 

## Allgemeine Ressourcen


* [Bash Reference Manual](https://www.gnu.org/software/bash/manual/bash.html)
  * Das offizielle Handbuch zur Bash.


* [Stackoverflow Übersicht](https://stackoverflow.com/tags/bash/info)
  * Enthält gute allgemeine Informationen zur Bash, sowie Tipps die Sie beachten sollten bevor Sie Fragen über Ihren Code irgendwo (auch im Moodle :)) posten.
  * Hat auch eine *Basic Syntax* und *Common Newbie Problems* Section.
  * Enthält auch Verweise zu Fragen auf Häufig gestellte Fragen die den Pattern: *Wie mache ich xyz?* oder *Warum macht xyz dies und das?* folgen.
  * Verweist auch noch auf weitere Literatur
  * Desweiteren finden Sie natürlich bei Stackoverflow eine große Menge an weiteren Antworten auf Fragen. In der Regel ist die Qualität der Antworten sehr hoch.

* [Advanced Bash-Scripting Guide](https://tldp.org/LDP/abs/html/) - An in-depth exploration of the art of shell scripting by Mendel Cooper
  * Sehr gute Literatur, enthält viele Beispiele und ist sehr umfassend. 
  * Gut als Nachschlagewerk für bestimmte Mechanismen in der Bash geeignet.

## Regular Expressions
Viel Power bekommen Tools wie grep und sed durch Regular Expressions (RegEx).
Leider ist der Syntax je nach Tool oder ggf. Programmiersprache immer etwas anders, das nervt ziemlich. Manchmal kann man die Art des RegEx durch Flags an das Programme ändern (hier am Besten in die man page schauen). Häufig findet man auch bereits RegEx Ausdrücke die etwas bestimmtes suchen, z.B. auf E-Mail Adressen matchen, hier muss man dann aufpassen ob man den RegEx 1:1 übernehmen kann.

Generell verwenden die meisten Bash Tools Basic Regular Expressions (BREs), häufig kann man auf die komfortableren Extended Regular Expressions (EREs) zurückgreifen (z.B. bei sed mit -r bzw. -E auf MacOS oder bei grep mit -E oder direkt über egrep). Eine elementare Unterscheidung von BRE zu ERE ist, ob Sie Metacharacters (also Anweisungen an die RegEx Engine mit einem \ escapen müssen).

Eine genaue Erklärung des Formats findet man in den man pages: [man 7 regex](https://man7.org/linux/man-pages/man7/regex.7.html)

Eine knappe Zusammenfassung und Unterscheidung von BRE vs. ERE findet man hier: [https://www.regular-expressions.info/posix.html](https://www.regular-expressions.info/posix.html)


Beide sind aber nicht equivalent zu Perl Compatible Regular Expression (PCRE), diese werden in vielen Bibliotheken verwendet. Die ERE gehen eher die Richtung von PCREs und viele PCRE lassen sich 1:1 nutzen, es kommt, wie so häufig auf die Details an und welche genauen Features ein Ausdruck verwendet.


Hier nun ein paar nützliche Ressourcen:
* [Stackoverflow Regex Übersicht](https://stackoverflow.com/tags/regex/info)
  * Common Problems and Pitfalls
  * Weitere Links auf Literatur zu RegEx
* [https://regex101.com/](https://regex101.com/)
  * Testen Sie einen RegEx und lassen Sie sich erklären welcher Teil wie genau auf einen Teststring matched
  * Praktisch um komplexere Ausdrücke zu verstehen oder selbige zu bauen
  * Bitte beachten Sie das die Seite keine BREs oder EREs kann, Sie können allerdings PCRE nutzen was häufig ähnlich zu ERE ist.
  * Es gibt eine vielzahl solcher Seiten, kennen Sie eine für BRE oder ERE? Lassen Sie es mich wissen!




## Jan's all time favorit Shell/Bash Tools

Hier ist mal eine Liste an Programmen die ich auf täglicher Basis nutze.
Es gibt noch viele andere Programme. Dies soll hier keine Liste von Programmen die man auswendig lernt werden. Die Reihenfolge ist Alphabetisch, weitere Informationen entnehmen Sie den man pages. Ich führe hier explizit keine wie ls, cd, etc auf.

* [awk](https://www.google.com/search?q=awk&sitesearch=man7.org%2Flinux%2Fman-pages) - pattern-directed scanning and processing language
* [cat](https://www.google.com/search?q=cat&sitesearch=man7.org%2Flinux%2Fman-pages) - Ausgabe von Dateien
* [curl](https://www.google.com/search?q=curl&sitesearch=man7.org%2Flinux%2Fman-pages) - URLs laden
* [cut](https://www.google.com/search?q=cut&sitesearch=man7.org%2Flinux%2Fman-pages) - cut out selected portions of each line of a file
* [date](https://www.google.com/search?q=date&sitesearch=man7.org%2Flinux%2Fman-pages) - display or set date and time
* [echo](https://www.google.com/search?q=echo&sitesearch=man7.org%2Flinux%2Fman-pages) - write arguments to the standard output
* [egrep](https://www.google.com/search?q=egrep&sitesearch=man7.org%2Flinux%2Fman-pages) - wie grep aber direkt mit EREs
* [git](https://www.google.com/search?q=git&sitesearch=man7.org%2Flinux%2Fman-pages) - git Versionskontrolle
* [grep](https://www.google.com/search?q=grep&sitesearch=man7.org%2Flinux%2Fman-pages) - file pattern searcher
  * `grep -rne ARG1 ARG2`

    ist ein Befehl den ich doch recht häufig verwende, er sucht den String/RegEx (-e) ARG1 rekursiv (-r) in allen Dateien unterhalb des Ordners der in ARG2 spezifiziert wird. -n gibt hier zusätlich noch die Zeile und die Datei an wo ein Match gefunden wurde. 
* [head](https://www.google.com/search?q=head&sitesearch=man7.org%2Flinux%2Fman-pages) - display first lines of a file
* [jq](https://stedolan.github.io/jq/) - Command-line JSON processor
* [less](https://www.google.com/search?q=less&sitesearch=man7.org%2Flinux%2Fman-pages) - erlaubt seitenweise Ansicht von stdin oder files sowie Scrolling
* [man](https://www.google.com/search?q=man&sitesearch=man7.org%2Flinux%2Fman-pages) - Manpages
* [nano](https://www.google.com/search?q=nano&sitesearch=man7.org%2Flinux%2Fman-pages) - Lightweight Datei Editor
* [parallel](https://www.gnu.org/software/parallel/man.html) - Führe Dinge parallel aus, ein bisschen wie xargs aber parallelisierend wenn man will
* [printf](https://www.google.com/search?q=printf&sitesearch=man7.org%2Flinux%2Fman-pages) - formattierte Ausgabe
* [scp](https://www.google.com/search?q=scp&sitesearch=man7.org%2Flinux%2Fman-pages) - Remote file transfer
* [screen](https://www.google.com/search?q=screen&sitesearch=man7.org%2Flinux%2Fman-pages) - Virtuelle Terminals innerhalb eines Terminals
* [sed](https://www.google.com/search?q=sed&sitesearch=man7.org%2Flinux%2Fman-pages) - Stream editor, RegEx suchen und ersetzen und mehr
* [seq](https://www.google.com/search?q=seq&sitesearch=man7.org%2Flinux%2Fman-pages) - Erzeugt sequenzen von Zahlen, z.B. für loops
* [sort](https://www.google.com/search?q=sort&sitesearch=man7.org%2Flinux%2Fman-pages) - Sortiert Zeilenweise auch sehr große Dateien
* [ssh](https://www.google.com/search?q=ssh&sitesearch=man7.org%2Flinux%2Fman-pages) - Remote Shell
* [tail](https://www.google.com/search?q=tail&sitesearch=man7.org%2Flinux%2Fman-pages) - display the last part of a file
* [tee](https://www.google.com/search?q=tee&sitesearch=man7.org%2Flinux%2Fman-pages) - copy stdin to file and pass on as stdout
* [time](https://www.google.com/search?q=time&sitesearch=man7.org%2Flinux%2Fman-pages) - Stoppuhr für andere Befehle (wie lange hat es gedauert mein Skript auszuführen?)
* [tr](https://www.google.com/search?q=tr&sitesearch=man7.org%2Flinux%2Fman-pages) - Buchstaben löschen oder ersetzen
* [uniq](https://www.google.com/search?q=uniq&sitesearch=man7.org%2Flinux%2Fman-pages) - report or filter out repeated lines in a file
* [wc](https://www.google.com/search?q=wc&sitesearch=man7.org%2Flinux%2Fman-pages) - Buchstaben, Wörter, Bytes, Zeilen zählen
* [xargs](https://www.google.com/search?q=xargs&sitesearch=man7.org%2Flinux%2Fman-pages) - Forloops über stdin um stdin auf Argumente zu mappen


