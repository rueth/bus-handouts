# Einstieg in die C-Programmierung

## Standards

* *"Der" C Standard* oder auch ISO/IEC 9899 definiert die Sprache
  * [C17 bei Perinorm](https://www.perinorm.com/results.aspx?q=AC%3aIX30553597) (über RWTH IP Addressbereich)
    * Dies ist der echte Standard, dieser kostet Geld. Sie haben vollen Zugriff über die [RWTH Universitätsbibliothek](https://www.ub.rwth-aachen.de/cms/UB/Forschung/Patent-und-Normenzentrum/Normen/Normenbestand/~htkm/Perinorm/) auf diesen und auf viele andere Standards und Normen.
  * [Release Status der aktuellen Version](http://www.open-std.org/jtc1/sc22/wg14/www/projects#9899)
    * Dort sind auch die final Drafts (aka. Vorversionen der echten Standards zu bekommen)
      * [C11 Draft](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1570.pdf)
      * [C17 Draft](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n2310.pdf)


* POSIX Standard bzw. IEEE Std 1003.1 definiert unter anderen die POSIX C Standard Bibliothek (diese spezifiziert etwas mehr als nur der C-Standard, quasi die POSIX spezifischen Interfaces noch) aber auch Threading sowie Systeminterfaces und command line tool Interfaces
  * Ähnlich wie der C Standard gibt es auch hier mehrere Revisionen
    * [IEEE Std 1003.1-2017](https://ieeexplore.ieee.org/document/8277153) erhalten Sie bei der IEEE wenn Sie über den RWTH IP Adressbereich zugreifen oder sich auf der Seite über RWTH Shibboleth (Single Sign-On) einloggen.

## Web

* [Stackoverflow Übersicht](https://stackoverflow.com/tags/c/info)
  * Enthält allgemeine Informationen über die Sprache
  * Hat ein FAQ das viele Anfängerfragen beantwortet (z.B. Unterschied zwischen ++i und i++)
  * Verweist auch noch auf weitere Online Inhalte
  * Ansonsten ist Ihnen die Suche innerhalb Stackoverflows ans Herz zu legen. Die Fragen und Antworten haben in der Regel eine sehr hohe Qualität
* [cdecl](https://cdecl.org/) - C gibberish ↔ English
  * Online Variante des [command line tools cdecl](https://linux.die.net/man/1/cdecl)
  * Übersetzt C deklarationen in lesbares Englisch
    * `int (*(*foo)(void ))[3]` --> `declare foo as pointer to function (void) returning pointer to array 3 of int` 

* [Online C Compiler von repilt.com](https://replit.com/languages/c)
  * Ganz nett um ein wenig mit verschiedenen Sprachfeatures zu experimentieren

# Debugging
Was tun wenn etwas nicht funktioniert?

## GDB
[The GNU Project Debugger](https://www.gnu.org/software/gdb/) ist ein weit verbreiteter debugger.
Sie können mit GDB ihr Programm ausführen und z.B. Breakpoints setzen oder sich Stacktraces anschauen.
GDB ist auch häufig in IDEs integriert kann aber genauso gut auf der Commandline ausgeführt werden.
GDB kann sich auch an bereits laufende Prozesse attachen oder Coredumps von abgestürzten Programmen anschauen. Sie sollten ihr Programm mit Debug Symbolen compilieren um bessere Ausgaben zu erhalten.

## Address Sanitizer (ASan)
Der [AddressSanitizer](https://github.com/google/sanitizers/wiki/AddressSanitizer) ist eine Compiler Instrumentation sowie eine Run-Time library. Was das heißt: Es wird zusätzlicher Code in Ihr Programm eingefügt um Speicherzugriffsverletzungen zu detektieren. ASan kostet Performance, im Schnitt ist ein 2x slowdown zu erwarten. Das heißt, nutzen Sie ASan zum debuggen bei der Entwicklung, i.d.r. wollen Sie es aber nicht im Release build ihres Programms.

Sie aktivieren ASan mit der compiler Option `-fsanitize=address`.
Es ist allerdings empfohlen auch noch `-O1` sowie `-fno-omit-frame-pointer` zu nutzen, um Performance sowie eine schönere Ausgabe zu bekommen. Weitere Informationen zu ASan und welche Optionen Sie noch haben finden Sie z.B. in der [CLANG Dokumentation](https://clang.llvm.org/docs/AddressSanitizer.html)


## Valgrind Memory Analyzer
[Valgrind](https://valgrind.org/) ist ein dynamisches Analysewerkzeug das versucht Memory und Threading Bugs zu finden. Sie können Valgrind i.d.r. über den Paketmanager ihres Systems installieren.
Diese [Anleitung bei Stackoverflow](https://stackoverflow.com/questions/5134891/how-do-i-use-valgrind-to-find-memory-leaks) gibt ein Beispiel wie Sie Valgrind einsetzen können.


## Fuzzing mit AFL
Häufig müssen Programme Daten verarbeiten, z.B. parsen. Diese Routinen, sind gerade in C sehr anfällig für Bugs, wenn z.B. fälschlicherweise über Arraygrenzen hinweg geschrieben wird.
Fuzzing ist eine populäre Methode ihren Code zu testen.
Hier sei erwähnt, dass Fuzzing keine vollständige Analyse ist und somit keine Garantien geben werden können, allerdings findet Fuzzing sehr viele Probleme in echter Software, weswegen es sehr beliebt ist.
Die grobe Idee ist, dass der Input randomisiert wird und man versucht Laufzeitfehler zu erzeugen.

Der [American Fuzzy Lop (AFL)](https://github.com/google/AFL) ist eins der bekanntesten Fuzzer Projekte. Wie AFL arbeitet können Sie der Projektbeschreibung entnehmen.
Dort finden Sie auch Anleitungen wie sie AFL nutzen


### CLANG's Statische Analyse
Statische Analyse erlaubt den Quellcode auf Fehler zu untersuchen.
CLANG's [Static Analyzer](https://clang-analyzer.llvm.org/) erlaubt es verschiedenste Arten von Fehlern im Code zu finden.
Dieser Analyzer ist auch Teil von XCode auf MacOS. 
Sie können ihn aber auch auf der command line unter Linux ausführen.
Eine Anleitung finden sie [hier](https://clang-analyzer.llvm.org/command-line.html).
Unter Ubuntu erhalten sie z.B. das scan-build Tool über das Paket `clang-tools`.

Das Programm erzeugt zum einen direkt eine Ausgabe auf der Konsole, sowie zusätzlich eine Visualizierung als HTML Dokument.