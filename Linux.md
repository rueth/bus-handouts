# Einstieg in Linux

An dieser Stelle möchte ich Sie auf die Arbeit des [Open Source Arbeitskreises](https://osak.fsmpi.rwth-aachen.de/index.html) der Fachschaft 1 aufmerksam machen.

## Welche Distribution ist die richtige?
Um Kontakt mit Linux zu bekommen ist es eigentlich egal welche Distribution Sie nutzen.
Für Ihre Entscheidung sollten Sie diese generellen Dinge beachten:

* Will ich ein Linux mit GUI oder reicht mir ein command-line interface?
  * Wenn GUI, welche möchte ich? Verschiedene Distributionen kommen mit verschiedenen default GUIs
  * Versionen ohne GUI sind meist als Server versionen vorhanden
* Aktualität der Programme vs. Stabilität der Programme
  * Es gibt meist verschiedene Versionen der selben Distribution die verschiedene Paketquellen nutzen.
 * ... 

Zu den verbreitesten Distributionen gehören:
* [Ubuntu](https://ubuntu.com/)
  * [Linux Mint](https://linuxmint.com/)
* [Arch Linux](https://archlinux.org/)
* [Fedora](https://getfedora.org/)
* [Debian](https://www.debian.org/)
* [OpenSUSE](https://www.opensuse.org/)
* [CentOS](https://www.centos.org/)
  
### Was nutze ich?
Meist Ubuntu als command-line only in der aktuellen long term support (LTS) version.


# Linux als Windows Nutzer
Als Windows Nutzer stehen Ihnen generell 3 Optionen zur Verfügung.
Die ersten beiden sind Virtualisierungslösungen.

## Option: WSL
Das Windows Subsystem for Linux (WSL) erlaubt es Linux Programme direkt aus Windows auszuführen.
Schauen Sie sich das [Video von Microsoft](https://www.youtube.com/watch?v=MrZolfGm8Zk) an für einen Überblick von WSL2.

Hier wird quasi eine ganze Linux Distribution in Windows installiert.
Hier sei anzumerken, dass Sie (im Moment) keinen GUI Desktop aus der Linux Distribution bekommen.
Sie bekommen hier quasi nur die command-line, diese aber schön integriert in Ihr Windows.

Eine Installationsanleitung finden Sie bei Microsoft: [Anleitung für WSL 2](https://docs.microsoft.com/en-us/windows/wsl/install-win10)


### Suboption: Docker
Docker Container bieten die Möglichkeit funktionalitäten zu Kapseln.
Z.B. können Sie einen Container [herunter laden](https://hub.docker.com/) der bereits alles was Sie zur C-Entwicklung benötigen zusammen mit einer bestimmten Linux Distribution bündelt.
Hier finden Sie z.B. Docker Container von [gcc](https://hub.docker.com/_/gcc).

Docker nutzt unter Windows dafür das WSL2, es ist quasi eine Voraussetzung.
Wenn Sie Docker einsetzen wollen, müssen Sie noch [Docker](https://www.docker.com/) selbst installieren.

## Option: Virtuelle Maschine
Mit virtuellen Maschinen können Sie sehr einfach Linux nutzen.
Diese emulieren einen ganzen Computer in ihrem Computer.

Sie benötigen hier eine Software die diese Virtuelle Maschinen bereit stellt, z.B. [Virtual Box](https://www.virtualbox.org/).

Sie können dann neue Maschinen erzeugen und dort z.B. von einem install image, wie Sie es bei den verschiedenen Distributionsherstellern bekommen, Linux installieren.
Hier sei zu betonen: Die Maschine ist vollständig isoliert von Ihrem Windows und Sie können sich nichts kaputt machen.

Meist möchte man etwas Komfort haben und z.B. die Bildschirmauflösung besser anpassen oder mit dem Windows Dateisystem interagieren. In der Regel bieten die hersteller der Virtuellen Maschinen dafür gesonderte Pakete für Linux an, bei Virtual Box heißen diese Guest Additions. Nachdem Sie diese in ihrem Linux in der Virtuellen Maschine installiert haben können Sie z.B. Ordner teilen.

Sie finden auch bereits installierte Virtuelle Maschinen die Sie sich herunterladen können.
z.B. auf [OS Boxes](https://www.osboxes.org/) erhalten Sie VirtualBox images. Achten Sie darauf das ggf. schon die Guest Additions installiert sind. 


## Option: Dual Boot
Sie können Linux parallel zu Ihrem Windows installieren.
Dafür benötigen Sie i.d.r. eine eigene Festplatte oder freien Speicherplatz den Sie partitionieren können.
Hier ist zu beachten, dass sie potentiell ihre Windows partition beschädigen können, wenn Sie etwas falsch machen. Machen Sie backups!

Die Installation selbst hängt von der gewählten Distribution ab, aber die Schritte sind meist immer die selben, hier ist eine [Anleitung für Linux Mint](https://itsfoss.com/guide-install-linux-mint-16-dual-boot-windows/).


# Linux als MacOS Nutzer
Da Sie bereits ein UNIX nutzen können Sie viele Dinge auch direkt aus MacOS machen.
Sollten Sie dennoch Linux nutzen wollen haben Sie 3 Optionen

## Option: Virtuelle Maschine
Genauso wie auf Windows können Sie auch hier Virtuelle Maschinen einsetzen.

Hier sei zu erwähnen das die neusten Macs mit ARM Prozessoren (Apple Silicon: M1, ...) ausgestattet sind. Virtual Box unterstützt diese zum aktuellen Zeitpunkt noch nicht.

[Parallels](https://www.parallels.com/) (kostenpflichtig) unterstützt bereits ARM. 

Generell sollten Sie, wenn Sie irgendeine Virtuelle Maschine wählen dann auch ein Linux Image nutzen, dass ihrer Prozessorarchitektur entspricht, also für die ARM Prozessoren in den Macs ist dass dann arm64.

## Option: Docker
Sie können unter MacOS problemlos Docker nutzen um ein Linux commandline in einem Container zu starten.
Einfach Docker installieren und geeigneten Container wählen oder selbst erstellen.

## Option: Dual Boot
Ein Dualboot ist technisch manchmal möglich. Hier kommt es stark auf das spezielle Gerät an.
Da Apple Hardware häufig nicht von der Stange kommt sind spezielle Kernelmodule oder Parameter nötig damit alles funktioniert. An dieser Stelle würde ich ihnen abraten diesen Weg zu gehen.


# Linux als Linux Nutzer

Hier müssen Sie nicht mehr viel tun :)


# Arbeiten mit Linux

Arbeiten mit Linux heißt auch arbeiten im Terminal.
Selbst wenn Sie ein Desktop Environment haben werden Sie immer wieder auf das Terminal zurück greifen.
Das heißt sie sollten eine Gewisse Menge an Standardbefehlen kennen um sich dort zurecht zu finden.

Hier mal eine alphabetische Auflistung von Basis Befehlen die mir spontan einfallen, schauen Sie mal ins [Bash](./Bash.md) Dokument für weitere Befehle:
* [cat](ttps://www.google.com/search?q=cat&sitesearch=man7.org%2Flinux%2Fman-pages) - Ausgabe von Dateien
* [cd](ttps://www.google.com/search?q=cd&sitesearch=man7.org%2Flinux%2Fman-pages) - wechselt das Arbeitsverzeichnis
* [chmod](ttps://www.google.com/search?q=chmod&sitesearch=man7.org%2Flinux%2Fman-pages) - Zugriffsrechte von Dateien/Ordnern anpassen
* [clear](ttps://www.google.com/search?q=clear&sitesearch=man7.org%2Flinux%2Fman-pages) - Bildschirminhalt löschen
* [df](ttps://www.google.com/search?q=df&sitesearch=man7.org%2Flinux%2Fman-pages) - freien Speicherplatz anzeigen (disk free)
* [file](ttps://www.google.com/search?q=file&sitesearch=man7.org%2Flinux%2Fman-pages) - zeigt Informationen zu einer Datei an
* [find](ttps://www.google.com/search?q=find&sitesearch=man7.org%2Flinux%2Fman-pages) - erlaubt das Suchen von Dateien
* [ip](ttps://www.google.com/search?q=ip&sitesearch=man7.org%2Flinux%2Fman-pages) - zur Verwaltung von Netzwerkaddressen
* [kill](ttps://www.google.com/search?q=kill&sitesearch=man7.org%2Flinux%2Fman-pages) - sendet Signale an Prozesse, z.B. zur Beendigung auffordern
* [ls](ttps://www.google.com/search?q=ls&sitesearch=man7.org%2Flinux%2Fman-pages) - listet Dateien/Ordner
* [lspci](ttps://www.google.com/search?q=lspci&sitesearch=man7.org%2Flinux%2Fman-pages) - zeigt alle PCI Geräte an
* [lsusb](ttps://www.google.com/search?q=lsusb&sitesearch=man7.org%2Flinux%2Fman-pages) - zeigt alle USB Geräte an
* [man](ttps://www.google.com/search?q=man&sitesearch=man7.org%2Flinux%2Fman-pages) - zeigt Erklärung zu Befehlen an
* [mkdir](ttps://www.google.com/search?q=mkdir&sitesearch=man7.org%2Flinux%2Fman-pages) - erzeugt Ordner 
* [ps](ttps://www.google.com/search?q=ps&sitesearch=man7.org%2Flinux%2Fman-pages) - zeigt laufende Prozesse an
* [pwd](ttps://www.google.com/search?q=pwd&sitesearch=man7.org%2Flinux%2Fman-pages) - gibt das aktuelle Arbeitsverzeichnis aus
* [su](ttps://www.google.com/search?q=su&sitesearch=man7.org%2Flinux%2Fman-pages) - Switch User, wechselt den aktuellen Benutzer
* [sudo](ttps://www.google.com/search?q=sudo&sitesearch=man7.org%2Flinux%2Fman-pages) - SuperUserDO, führt einen Befehl mit root rechten aus siehe auch `man sudoers`
* [top](ttps://www.google.com/search?q=top&sitesearch=man7.org%2Flinux%2Fman-pages) - interaktiver Prozessviewer, siehe auch [htop](ttps://www.google.com/search?q=htop&sitesearch=man7.org%2Flinux%2Fman-pages)
* [touch](ttps://www.google.com/search?q=touch&sitesearch=man7.org%2Flinux%2Fman-pages) - erzeugt eine Datei wenn sie nicht existiert und setzt das last access Date einer Datei auf jetzt 
* [which](ttps://www.google.com/search?q=which&sitesearch=man7.org%2Flinux%2Fman-pages) - löst $PATH auf und zeigt an was welcher Befehl genau ausgeführt wird
* [who](ttps://www.google.com/search?q=who&sitesearch=man7.org%2Flinux%2Fman-pages) - welche Nutzer sind angemeldet?
* [whoami](ttps://www.google.com/search?q=whoami&sitesearch=man7.org%2Flinux%2Fman-pages) - welcher Nutzer bin ich gerade selbst?



## Desktop Environment oder Window Manager
Die Grafische Oberfläche bzw. der Window Manager ist die grafische Ausgabe ihres Linux Desktops, also das was Fenster managed, ihnen einen Dateimanager biete, Programme auflistet etc. in Summe kann man es als Desktop Environment bezeichnen.
Es gibt viele verschiedene.
Egal mit welchem Desktop Environment ihre Distribution kommt, Sie können in der Regel ein anderes nach-installieren. Hier sei darauf hingewiesen, das viele Distributionen quasi genau nur das sind, nämlich das Austauschen des default Window Managers in einer ansonsten weit verbreiteten Distribution, z.B. Kubuntu ist Ubuntu mit KDE oder MATE ist ein Ubuntu mit Gnome 2.x. Da das installieren eines anderen Window Manager manchmal nicht ganz Problemfrei ist kann es sich also anbieten vorher mal zu gucken was man gern hätte.

Hier mal die wichtigsten die ich auch mal benutzt habe (alphabetisch):

* [GNOME](https://www.gnome.org/) - War lange Zeit default bei Ubuntu, default in Fedora und Debian, also recht verbreitet
  * GNOME 3 sieht fundamental anders aus und hat ein anderes Bedienkonzept als GNOME 2, beide werden noch weiterentwickelt.
* [KDE](https://kde.org/) - Recht ressourcen-intensiv, aber dafür ?hübsch?, erinnert ein wenig an Windows
* [Unity](https://wiki.ubuntuusers.de/Unity/) - Default in Ubuntu. 
* [Xfce](https://www.xfce.org/) - Sehr ressourcen-schonend und minimalistisch.
  
Einen Vergleich bzw. eine Übersicht finden Sie z.B. [hier](https://renewablepcs.wordpress.com/about-linux/kde-gnome-or-xfce/) oder via [Google](https://letmegooglethat.com/?q=best+linux+desktop+environment).

## Dateisystem

Ihre Festplatte bzw. Partitionen auf der Festplatte können verschiedene Dateisysteme nutzen. Diese haben verschiedene Eigenschaften. Meist ist man mit den Vorgaben der Distribution gut bedient.

Weit verbreitete Dateisystemformate (alphabetisch):
* brtfs
* exFAT
* ext2
* ext3
* ext4
* NTFS
* raiserfs
* xfs
* zfs

### Mounten von Dateisystemen
Unter Linux werden Dateisysteme einfach irgendwo in die Baumstruktur eingehangen (mounting).
Sie benötigen quasi nur einen leeren Ordner in dem die Dateien und Ordner eines Dateisystems erscheinen sollen.

Hier mal ein Beispiel. Angenommen wir haben eine Festplatte als `/dev/sdb` und diese hat eine ext4 Partition z.B. `/dev/sdb2`.
Wir haben eine leeren Ordner in `/mnt/here` erstellt (`/mnt` ist so ein Ort in den man nach Konvention mounts legen kann, aber das ist kein muss).

Mit `mount -t ext4 /dev/sdb2 /mnt/here` können wir das Dateisystem einhängen (i.d.r. braucht man root Rechte, mit -t wird hier der Dateisystem Typ spezifiziert, i.d.r. ist `mount` aber in der Lage das auch gut zu raten und sie brauchen es nicht). 

Mit `unmount /mnt/here` oder `unmount /dev/sdb2` können wir das wieder Rückgängig machen.

Die Datei `/etc/fstab` enthält Standardmounts, z.B. wie `/` gemounted wird (siehe `man fstab` für Details).

Die Datei `/etc/mtab` enthält alle aktuell gemounteten Dateisysteme.


## Paketmanger
Verschiedene Distributionen haben verschiedene Paketmanager. Diese haben auch häufig eine GUI. Der Paketmanager lässt sich aber auch immer über das Terminal steuern.

### APT
APT ist der Paketmanager auf Debian basierten Systemen, wie z.B. auch Ubuntu.
Sie benötigen i.d.r. root Rechte um apt zu bedienen (sudo).

Wichtige Befehle:
* `apt-get moo`
* `apt-get update` Refreshed die Paketquellen (siehe `/etc/apt/sources.list`)
* `apt-get install [Packagename]` installiert das angegebene Paket in `[Packagename]` und dessen Abhängigkeiten
* `apt-get remove [Packagename]` deinstalliert das angegebene Pakete in `[Packagename]` 
* `apt-cache search [SUCHE]` durchsucht Paketnamen nach `[SUCHE]` 
* `apt-file find [SUCHE]` sucht pakete die Dateien mit den namen `[SUCHE]` enthalten.
  * Sie müssen zuvor `apt-file` installieren (`apt-get install apt-file`) und mit `apt-file update` die interne Datenbank refreshen.

### Pacman
Pacman ist der Paketmanager bei Arch Linux.
Da Arch eine außergewöhnlich gute Dokumentation besitzt verweise ich hier mal auf das [Wiki zu pacman](https://wiki.archlinux.org/title/pacman).

#### Was tun wenn irgendwas kaputt geht? (APT)
Ja, sowas kann mal passieren und ist sehr nervig.
Sowas passiert besonders gern, wenn einem Mitten in der Installation z.B. der Computer abstürzt oder die Festplatte voll läuft oder aber auch wenn man Paketquellen gemischt hat die für eine falsche Distribution oder Version sind (passiert schonmal nach nem Major Update oder wenn man nicht aufpasst).

Ein Pauschalrezept, was immer mit Garantie funktioniert gibt es nicht.
Man kann versuchen eine abgebrochene Installation mit dpkg (sudo!) fortzusetzen, `dpkg –configure -a` oder `apt-get -f install`.

Falls das nicht klappt hilft häufig nur zu versuchen die Pakete erstmal zu deinstallieren.
Hier kann es besonders tricky werden, wenn man ein sehr elementares Paket löschen muss, dass viele andere Pakete als Abhängigkeit haben.
Daher hier genau schauen, was wird hier alles gelöscht wenn man es deinstalliert!
Ggf. Pakete merken um sie im Anschluss neu zu installieren!
In solchen Fällen, dass sehr viel deinstalliert wird und man vielleicht gar nicht genau weiß was die Pakete alles tun ist es wichtig erstmal die Ursache gefunden zu haben, so dass man sich auch sicher sein kann das Problem zu lösen und nicht mit einem halb deinstalliertem System am Ende da zu stehen.
Hier sollte man immer schauen, welche Version eines Pakets macht genau das Problem, aus welcher Paketquelle kommt es? Ist das eine offizielle Quelle? Ggf. inoffizielle Quellen deaktivieren (`apt-get update` nicht vergessen!)! Google Suche bemühen, vielleicht haben Sie ein bekanntes Problem!

## ??? Was fehlt hier ???
