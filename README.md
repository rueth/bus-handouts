# Betriebssysteme und Systemsoftware - Handouts

In diesem Repository sammeln wir nützliche Informationen zu den Themen der Vorlesung.

Primär ist dies als Tipps und Link Sammlung zu weiterführenden Inhalten zu Linux, Bash und C-Programmierung gedacht.


Wenn Sie selbst Inhalte kennen, die Sie hier gerne sehen würden, stellen Sie einen merge/pull-request gegen dieses Repository.
Wenn Sie sich noch nicht mit Versionskontrollsystemen wie git auskennen lege ich Ihnen folgende Tutorials nah.

Wenn Sie nur einen groben Überblick über die Befehle haben wollen: [hier](https://rogerdudler.github.io/git-guide/)
Für ein paar Informationen wie git und gitlab zusammen hängen: [hier](https://towardsdatascience.com/getting-started-with-gitlab-the-absolute-beginners-guide-ea9e5cadac8b)



# [Linux](./Linux.md)
# [Bash](./Bash.md)
# [C-Programmierung](./C.md)
